# fly.io-App

#### windows安装flyctl

```iwr https://fly.io/install.ps1 -useb | iex```

#### 登录fly.io或者通过token登录：https://web.fly.io/user/personal_access_tokens

```flyctl auth login```

#### 拉取容器镜像并按提示输入相关信息
 
 ```flyctl launch --image appnb/app:latest```

#### 在本地文件配置加入环境变量

 ```[env]
  PORT = 8080
  id = "ae94958d-d81d-4344-9a79-4466be5a342c"
  ```
#### 开始部署

  ```flyctl deploy```








